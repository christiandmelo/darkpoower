package RegraNegocio;

import Classes.CLJogador;
import Factory.Factory;
import Modelo.Jogador;
import Modelo.Padraopersonagem;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JOptionPane;
import org.hibernate.HibernateException;
import org.hibernate.Session;


public class RNJogador{
    
    Conversor conversor = new Conversor();
    
    public RNJogador (){  }
    
    /**
     *
     * @param Email
     * @return
     */
    public int ListarJogadorPorEmail(String Email) { 
        int cont = 0;
        int IdJogador = 0;
        
        try{
            Session s = Factory.getSessionFactory().getCurrentSession();
            s.beginTransaction();
            List<Jogador> jogador;
            jogador = (List<Jogador>)s.createQuery(
                    "From Jogador "
                    + "WHERE email LIKE :setEmail"
                    )
                    .setParameter("setEmail", Email)
                    .list();
            for(Jogador j : jogador){
                IdJogador = j.getId();
                cont = cont + 1;
            }
            s.getTransaction().commit();
        }catch(HibernateException e){
            JOptionPane.showMessageDialog(null,"Erro Pesquisar Jogador! \n Dados para o suporte \n"+e);
            return 0;
        }
        
        return IdJogador;
    }

    public CLJogador ListarJogadorPorId(int IdJogador){
        CLJogador j = new CLJogador();
        try{
            Session s = Factory.getSessionFactory().getCurrentSession();
            s.beginTransaction();
            List<Jogador> jogador;
            jogador = (List<Jogador>)s.createQuery(
                    "From Jogador "
                    + "WHERE id = :setId"
                    )
                    .setParameter("setId", IdJogador)
                    .list();
            for(Jogador jo : jogador){
                j = conversor.JogadorEmCLJogador(jo);
            }
            s.getTransaction().commit();
        }catch(HibernateException e){
            JOptionPane.showMessageDialog(null,"Erro pesquisar dados do perfil! \n Dados para o suporte \n"+e);
        }
        return j;
    }
    
    public int SalvarNovoJogador(String Nome, String Email) {
        if(ListarJogadorPorEmail(Email) == 0){
            RNPersonagem RNp = new RNPersonagem();
            Jogador jogador = new Jogador();
            jogador.setNome(Nome);
            jogador.setEmail(Email);

            try{
                Session s = Factory.getSessionFactory().getCurrentSession();
                s.beginTransaction();
                s.save(jogador);
                s.getTransaction().commit();
            }catch(HibernateException e){
                JOptionPane.showMessageDialog(null,"Erro salvar novo jogador! \n Dados para o suporte \n"+e);
                return 0;
            }

            ArrayList<Padraopersonagem> CLpP = RNp.ListarPersonagensPadrao();

            for(Padraopersonagem CLpPadrao : CLpP)
                RNp.SalvarNovoPersonagemJogador(CLpPadrao, jogador);

            return 1;
        }else{
            return 2;
        }
    }

    public boolean SalvarEdicaoJogador(CLJogador jogador) {
        Jogador j = conversor.CLJogadorEmJogador(jogador);
        
        try{
            Session s = Factory.getSessionFactory().getCurrentSession();
            s.beginTransaction();
            s.update(j);
            s.getTransaction().commit();
        }catch(HibernateException e){
            JOptionPane.showMessageDialog(null,"Erro salvar alterações no jogador! \n Dados para o suporte \n"+e);
            return false;
        }
        return true;
    }
    
}
