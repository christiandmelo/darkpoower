package RegraNegocio;

import Classes.CLPersonagem;
import Factory.Factory;
import Modelo.Jogador;
import Modelo.Padraopersonagem;
import Modelo.Padraopersonagemarma;
import Modelo.Padraopersonagemmagia;
import Modelo.Personagem;
import Modelo.Personagemarma;
import Modelo.Personagemmagia;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JOptionPane;
import org.hibernate.HibernateException;
import org.hibernate.Session;


public class RNPersonagem{
    Conversor conversor = new Conversor();
    
    public RNPersonagem (){
    }
    
    // <editor-fold defaultstate="collapsed" desc="Lista Personagem Padrão">
    private List<Padraopersonagemarma> ListarArmasPadrao(Padraopersonagem PersonagemPadrao) {
        List<Padraopersonagemarma> armasPadrao = null;
        try{
            Session s = Factory.getSessionFactory().getCurrentSession();
            s.beginTransaction();
            armasPadrao = (List<Padraopersonagemarma>)s.createQuery(
                    "From Padraopersonagemarma as padrao "
                    + "inner join fetch padrao.padraoarma "
                    + "WHERE padrao.padraopersonagem = :setId"
                    )
                    .setParameter("setId", PersonagemPadrao)
                    .list();
            s.getTransaction().commit();
        }catch(HibernateException e){
            JOptionPane.showMessageDialog(null,"Erro pesquisar as armas! \n Dados para o suporte \n"+e);
        }
        return armasPadrao;
    }

    private List<Padraopersonagemmagia> ListarMagiasPadrao(Padraopersonagem PersonagemPadrao) {
        List<Padraopersonagemmagia> magiasPadrao = null;
        try{
            Session s = Factory.getSessionFactory().getCurrentSession();
            s.beginTransaction();
            magiasPadrao = (List<Padraopersonagemmagia>)s.createQuery(
                    "From Padraopersonagemmagia padrao "
                    + "inner join fetch padrao.padraomagia "
                    + "WHERE padrao.padraopersonagem = :setId"
                    )
                    .setParameter("setId", PersonagemPadrao)
                    .list();
            s.getTransaction().commit();
        }catch(HibernateException e){
            JOptionPane.showMessageDialog(null,"Erro pesquisar as magias! \n Dados para o suporte \n"+e);
        }
        return magiasPadrao;
    }
    
    
    public ArrayList<Padraopersonagem> ListarPersonagensPadrao(){
        List<Padraopersonagem> personagens = null;
        try{
            Session s = Factory.getSessionFactory().getCurrentSession();
            s.beginTransaction();
            
            personagens = (List<Padraopersonagem>)s.createQuery(
                    "From Padraopersonagem "
                    )
                    .list();
            s.getTransaction().commit();
        }catch(HibernateException e){
            JOptionPane.showMessageDialog(null,"Erro pesquisar os personagens padrão, gentileza entrar em contato com o suporte! \n Dados para o suporte \n"+e);
        }
                
        
        return (ArrayList<Padraopersonagem>) personagens;
    }
    // </editor-fold>
    
    
    
    
    
    
    
    // <editor-fold defaultstate="collapsed" desc="Lista Personagem">
    private List<Personagemarma> ListarArmasPorPersonagem(Personagem Personagem) {
        List<Personagemarma> armas = null;
        try{
            Session s = Factory.getSessionFactory().getCurrentSession();
            s.beginTransaction();
            armas = (List<Personagemarma>)s.createQuery(
                    "From Personagemarma pa "
                    + "inner join fetch pa.padraoarma "
                    + "WHERE pa.personagem = :setId "
                    )
                    .setParameter("setId", Personagem)
                    .list();
            s.getTransaction().commit();
        }catch(HibernateException e){
            JOptionPane.showMessageDialog(null,"Erro pesquisar as armas! \n Dados para o suporte \n"+e);
        }
        return armas;
    }

    private List<Personagemmagia> ListarMagiasPorPersonagem(Personagem Personagem) {
        List<Personagemmagia> magias = null;
        try{
            Session s = Factory.getSessionFactory().getCurrentSession();
            s.beginTransaction();
            magias = (List<Personagemmagia>)s.createQuery(
                    "From Personagemmagia pm "
                    + "inner join fetch pm.padraomagia "
                    + "WHERE pm.personagem = :setId "
                    )
                    .setParameter("setId", Personagem)
                    .list();
            s.getTransaction().commit();
        }catch(HibernateException e){
            JOptionPane.showMessageDialog(null,"Erro pesquisar as magias! \n Dados para o suporte \n"+e);
        }
        return magias;
    }
    
    
    public ArrayList<CLPersonagem> ListarPersonagensPorJogador(Jogador jogador){
        ArrayList<CLPersonagem> p = new ArrayList<>();
        List<Personagem> personagens = null;
        try{
            Session s = Factory.getSessionFactory().getCurrentSession();
            s.beginTransaction();
            personagens = (List<Personagem>)s.createQuery(
                    "From Personagem pm "
                    + "inner join fetch pm.padraopersonagem "
                    + "WHERE pm.jogador = :setId "
                    )
                    .setParameter("setId", jogador)
                    .list();
            s.getTransaction().commit();
        }catch(HibernateException e){
            JOptionPane.showMessageDialog(null,"Erro pesquisar os personagens! \n Dados para o suporte \n"+e);
        }
        
        for(Personagem pe : personagens){
                List<Personagemarma> pea = ListarArmasPorPersonagem(pe);
                List<Personagemmagia> pem = ListarMagiasPorPersonagem(pe);
                p.add(conversor.PersonagemEmCLPersonagem(pe,pea,pem));
            }
        return p;
    }
    //</editor-fold>
    
    
    
    
    
    
    // <editor-fold defaultstate="collapsed" desc="Edição Personagem">
    public boolean SalvarNovoPersonagemJogador(Padraopersonagem clp, Jogador jogador) {
        CLPersonagem clpersonagem;
        Personagem personagem;
        ArrayList<Personagemarma> personagemA = new ArrayList<>();
        ArrayList<Personagemmagia> personagemM = new ArrayList<>();
        
        //converte para personagem e tentar salvar no banco de dados
        personagem = conversor.PadraopersonagemEmPersonagem(clp, jogador, false);
        try{
            Session s = Factory.getSessionFactory().getCurrentSession();
            s.beginTransaction();
            s.save(personagem);
            s.getTransaction().commit();
        }catch(HibernateException e){
            JOptionPane.showMessageDialog(null,"Erro salvar personagem! \n Dados para o suporte \n"+e);
            return false;
        }
        
        //passa por todas as armas padrao, converte para personagemarma e tenta salva no banco de dados
        List<Padraopersonagemarma> personagemarmas = ListarArmasPadrao(clp);
        for(Padraopersonagemarma paa : personagemarmas){
            Personagemarma a = conversor.PadraopersonagemarmaEmPersonagemarma(paa, personagem, false);
            try{
                Session s = Factory.getSessionFactory().getCurrentSession();
                s.beginTransaction();
                s.save(a);
                s.getTransaction().commit();
            }catch(HibernateException e){
                JOptionPane.showMessageDialog(null,"Erro salvar arma personagem! \n Dados para o suporte \n"+e);
                return false;
            }
            personagemA.add(a);
        }
        
        List<Padraopersonagemmagia> personagemmagias = ListarMagiasPadrao(clp);
        for(Padraopersonagemmagia ppm : personagemmagias){
            Personagemmagia m = conversor.PadraopersonagemmagiaEmPersonagemmagia(ppm, personagem, false);
            try{
                Session s = Factory.getSessionFactory().getCurrentSession();
                s.beginTransaction();
                s.save(m);
                s.getTransaction().commit();
            }catch(HibernateException e){
                JOptionPane.showMessageDialog(null,"Erro salvar magia personagem! \n Dados para o suporte \n"+e);
                return false;
            }   
            personagemM.add(m);
        }
        
        return true;
        
    }
    // </editor-fold>
    
}
