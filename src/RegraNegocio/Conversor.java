/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package RegraNegocio;

import Classes.CLArma;
import Classes.CLJogador;
import Classes.CLMagiaCura;
import Classes.CLMagiaDano;
import Classes.CLPartidaJogador;
import Classes.CLPersonagem;
import Modelo.Jogador;
import Modelo.Padraoarma;
import Modelo.Padraomagia;
import Modelo.Padraopersonagem;
import Modelo.Padraopersonagemarma;
import Modelo.Padraopersonagemmagia;
import Modelo.Personagem;
import Modelo.Personagemarma;
import Modelo.Personagemmagia;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author 
 */
public class Conversor {
    
    public Conversor(){}
    
    // <editor-fold defaultstate="collapsed" desc="Jogador">
    public CLJogador JogadorEmCLJogador(Jogador jo){
        CLJogador clj = new CLJogador();
        clj.setId(jo.getId());
        clj.setNome(jo.getNome());
        clj.setEmail(jo.getEmail());
        
        return clj;
    }
    
    public Jogador CLJogadorEmJogador(CLJogador clj){
        Jogador jo = new Jogador();
        if(clj.getId() != 0) jo.setId(clj.getId());
            
        jo.setNome(clj.getNome());
        jo.setEmail(clj.getEmail());
                
        return jo;
    }
    
    
    public CLPartidaJogador CLJogadorEmCLPartidaJogador(CLJogador jo){
        CLPartidaJogador cPartidaJogador = new CLPartidaJogador();
        ArrayList<CLPersonagem> clp = new ArrayList<>();
        
        cPartidaJogador.setId(jo.getId());
        cPartidaJogador.setNome(jo.getNome());
        cPartidaJogador.setEmail(jo.getEmail());
        for (CLPersonagem p : jo.getListaPersonagens()){
            clp.add(p);
        }
        
        cPartidaJogador.setListaPersonagens(clp);
            
        return cPartidaJogador;
    }
    //</editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="Personagem Padrão">
    public Personagem PadraopersonagemEmPersonagem(Padraopersonagem pp, Jogador jogador, boolean setId){
        Personagem p = new Personagem();
        
        if(setId) p.setId(pp.getId());
        p.setJogador(jogador);
        p.setPontosDeVida(pp.getPontosDeVida());
        p.setPontosDeMana(pp.getPontosDeMana());
        p.setForcaFisica(pp.getForcaFisica());
        p.setForcaMagica(pp.getForcaMagica());
        p.setResistenciaFisica(pp.getResistenciaFisica());
        p.setResistenciaMagica(pp.getResistenciaMagica());
        p.setAgilidade(pp.getAgilidade());
        p.setPadraopersonagem(pp);
        
        return p;
    }
    
    public Personagemarma PadraopersonagemarmaEmPersonagemarma(Padraopersonagemarma paa, Personagem p, boolean setId){
        Personagemarma pa = new Personagemarma();
        
        if(setId) pa.setId(paa.getPadraoarma().getId());
        pa.setDano(paa.getPadraoarma().getDano());
        pa.setPersonagem(p);
        
        Padraoarma armap = new Padraoarma(paa.getPadraoarma().getId(),paa.getPadraoarma().getNome(),paa.getPadraoarma().getDano());
        pa.setPadraoarma(armap);   
        
        return pa;
    }
    
    /**
     *
     * @param ppm
     * @param p
     * @param setId
     * @return
     */
    public Personagemmagia PadraopersonagemmagiaEmPersonagemmagia(Padraopersonagemmagia ppm, Personagem p, boolean setId){
        Personagemmagia pm = new Personagemmagia();
        
        if(setId) pm.setId(ppm.getPadraomagia().getId());
        pm.setCura(ppm.getPadraomagia().getCura());
        pm.setDano(ppm.getPadraomagia().getDano());
        pm.setPersonagem(p);
        
        Padraomagia magiap = new Padraomagia(ppm.getPadraomagia().getId(),ppm.getPadraomagia().getNome(),ppm.getPadraomagia().getDano(),ppm.getPadraomagia().getCura(),ppm.getPadraomagia().getGastoDeMana(),ppm.getPadraomagia().getTipo());
        pm.setPadraomagia(magiap);
        
        return pm;
    }
    //</editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="Personagem">
    public CLPersonagem PersonagemEmCLPersonagem(Personagem p, List<Personagemarma> pArma, List<Personagemmagia> pMagia){
        CLPersonagem clp = new CLPersonagem(p.getPontosDeVida(), p.getPontosDeMana());
        
        clp.setId(p.getId());
        clp.setNome(p.getPadraopersonagem().getNome());
        clp.setPontosDeVida(p.getPontosDeVida());
        clp.setPontosDeMana(p.getPontosDeMana());
        clp.setForcaFisica(p.getForcaFisica());
        clp.setForcaMagica(p.getForcaMagica());
        clp.setTipo(p.getPadraopersonagem().getTipo());
        clp.setIdPadrao(p.getPadraopersonagem().getId());
        clp.setResistenciaFisica(p.getResistenciaFisica());
        clp.setResistenciaMagica(p.getResistenciaMagica());
        clp.setAgilidade(p.getAgilidade());
        
        for(Personagemarma pa : pArma){
            CLArma cla = new CLArma();
            cla.setId(pa.getId());
            cla.setNome(pa.getPadraoarma().getNome());
            cla.setDano(pa.getDano());
            
            clp.setListaArmas(cla);
        }
        
        for(Personagemmagia pm : pMagia){
            CLMagiaCura clmc = new CLMagiaCura();
            CLMagiaDano clmd = new CLMagiaDano();
            
            
            if(pm.getCura() != null){
                clmc.setId(pm.getId());
                clmc.setNome(pm.getPadraomagia().getNome());
                clmc.setGastoDeMana(pm.getPadraomagia().getGastoDeMana());
                clmc.setCura(pm.getCura());
                clp.setListaCura(clmc);
            }else{
                clmd.setId(pm.getId());
                clmd.setNome(pm.getPadraomagia().getNome());
                clmd.setGastoDeMana(pm.getPadraomagia().getGastoDeMana());
                clmd.setDano(pm.getDano());
                clp.setListaDano(clmd);
            }
        }
        
        return clp;
    }
    //</editor-fold>
    
}
