package RegraNegocio;

import Factory.Factory;
import Modelo.Partida;
import javax.swing.JOptionPane;
import org.hibernate.HibernateException;
import org.hibernate.Session;


public class RNPartida{
    
    public RNPartida (){
        
    }
    
    public boolean SalvarNovaPartida(){
        Partida partida = new Partida();
        
        try{
            Session s = Factory.getSessionFactory().getCurrentSession();
            s.beginTransaction();
            s.save(partida);
            s.getTransaction().commit();
        }catch(HibernateException e){
            JOptionPane.showMessageDialog(null,"Erro iniciar nova partida! \n Dados para o suporte \n"+e);
            return false;
        }
        return true;
    }
    
}
