package Classes;

public abstract class CLMagia {


     private int id;
     private String nome;
     private int gastoDeMana;
   
    public Integer getId() {
        return this.id;
    }
    
    public void setId(int id){
        this.id = id;
    }
    
    public String getNome() {
        return this.nome;
    }
    
    public void setNome(String nome){
        this.nome = nome;
    }
    
    public int getGastoDeMana() {
        return this.gastoDeMana;
    }
    
    public void setGastoDeMana(int gasto){
        this.gastoDeMana = gasto;
    }
}


