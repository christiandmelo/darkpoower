package Classes;

import java.util.ArrayList;
import java.util.List;


public class CLJogador{


     private int id;
     private String nome;
     private String email;
     private List<CLPersonagem> listaPersonagens;
     
    public CLJogador(){
        this.listaPersonagens = new ArrayList<>();
    }

    public CLJogador(Integer id, String nome, String email) {
        this.id = id;
        this.nome = nome;
        this.email = email;
        this.listaPersonagens = new ArrayList<>();
    }
   
    public int getId() {
        return this.id;
    }
    
    public void setId(int id){
        this.id = id;
    }

    public String getNome() {
        return this.nome;
    }
    
    public void setNome(String nome) {
        this.nome = nome;
    }
    
    public String getEmail() {
        return this.email;
    }
    
    public void setEmail(String email) {
        this.email = email;
    }
    
    public List<CLPersonagem> getListaPersonagens() {
        return this.listaPersonagens;
    }
    
    public CLPersonagem getPersonagem(int indexPersonagem){
        return this.listaPersonagens.get(indexPersonagem);
    }

    public void setListaPersonagens(List<CLPersonagem> personagem) {
        this.listaPersonagens = personagem;
    }
    
    public void setListaPersonagens(CLPersonagem personagem) {
        this.listaPersonagens.add(personagem);
    }
    
    public int countPersonagens(){
        return this.listaPersonagens.size();
    }
    
    public void removePersonagem(int index){
        this.listaPersonagens.remove(index);
    }
    
    /*public void setAtualizaPersonagem(int index, CLPersonagem personagem){
        this.listaPersonagens.set(index, personagem);
    }*/
}


