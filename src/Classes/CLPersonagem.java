package Classes;

import java.util.ArrayList;
import java.util.List;

public class CLPersonagem  {
    
    private int id;
    private String nome;
    private int pontosDeVida;
    private int pontosDeVidaPerdidos;
    private int pontosDeMana;
    private int pontosDeManaPerdidos;
    private int ataqueEspecial;
    private int forcaFisica;
    private int forcaMagica;
    private int resistenciaFisica;
    private int resistenciaMagica;
    private int agilidade;
    private String tipo;
    private int idPadrao;
    private List<CLArma> listaArmas;
    private List<CLMagiaCura> listaCura;
    private List<CLMagiaDano> listaDano;
     
    public CLPersonagem(){
        this.ataqueEspecial = 0;
        this.listaArmas = new ArrayList<>();
        this.listaCura = new ArrayList<>();
        this.listaDano = new ArrayList<>();
    }
    
    public CLPersonagem(int vidaPerdida, int manaPerdida){
        this.ataqueEspecial = 0;
        this.pontosDeVidaPerdidos = vidaPerdida;
        this.pontosDeManaPerdidos = manaPerdida;
        this.listaArmas = new ArrayList<>();
        this.listaCura = new ArrayList<>();
        this.listaDano = new ArrayList<>();
    }

    public int getId() {
        return id;
    }
    
    public void setId(int id){
        this.id = id;
    }
    
    public String getNome() {
        return nome;
    }
    
    public void setNome(String nome){
        this.nome = nome;
    }

    public int getPontosDeVida() {
        return pontosDeVida;
    }

    public void setPontosDeVida(int pontosDeVida) {
        this.pontosDeVida = pontosDeVida;
    }
    
    public int getPontosDeVidaPerdidos() {
        return pontosDeVidaPerdidos;
    }
    
    public void setPontosDeVidaPerdidosPositivo(int pontos) {
        int p = this.pontosDeVidaPerdidos + pontos;
        if(p >= this.pontosDeVida){
            this.pontosDeVidaPerdidos = this.pontosDeVida;
        }else{
            this.pontosDeVidaPerdidos = p;
        }
    }

    public boolean setPontosDeVidaPerdidosNegativo(int pontos) {
        if(pontos <= 0){
            return true;
        }else{
            int p = this.pontosDeVidaPerdidos - pontos;
            if(p <= 0){
                this.pontosDeVidaPerdidos = 0;
                return false;
            }else{
                this.pontosDeVidaPerdidos = p;
                return true;
            }
        }
    }

    public int getPontosDeMana() {
        return pontosDeMana;
    }

    public void setPontosDeMana(int pontosDeMana) {
        this.pontosDeMana = pontosDeMana;
    }
    
    public int getPontosDeManaPerdidos() {
        return pontosDeManaPerdidos;
    }

    public void setPontosDeManaPerdidosPositivo(int pontos) {
        int p = this.pontosDeManaPerdidos + pontos;
        if(p >= this.pontosDeMana){
            this.pontosDeManaPerdidos = this.pontosDeMana;
        }else{
            this.pontosDeManaPerdidos = p;
        }
    }
    
    public void setPontosDeManaPerdidosNegativos(int pontos) {
        int p = this.pontosDeManaPerdidos - pontos;
        if(p <= 0){
            this.pontosDeManaPerdidos = 0;
        }else{
            this.pontosDeManaPerdidos = p;
        }
    }
    
     public int getAtaqueEspecial() {
        return ataqueEspecial;
    }

    public void setAtaqueEspecialPositivo() {
        int p = this.ataqueEspecial + 1;
        if(p <= 10){
            this.ataqueEspecial = p;
        }
    }
    
    public void setAtaqueEspecialNegativo() {
        this.ataqueEspecial = 0;
    }

    public int getForcaFisica() {
        return forcaFisica;
    }

    public void setForcaFisica(int forcaFisica) {
        this.forcaFisica = forcaFisica;
    }

    public int getForcaMagica() {
        return forcaMagica;
    }

    public void setForcaMagica(int forcaMagica) {
        this.forcaMagica = forcaMagica;
    }

    public int getResistenciaFisica() {
        return resistenciaFisica;
    }

    public void setResistenciaFisica(int resistenciaFisica) {
        this.resistenciaFisica = resistenciaFisica;
    }

    public int getResistenciaMagica() {
        return resistenciaMagica;
    }

    public void setResistenciaMagica(int resistenciaMagica) {
        this.resistenciaMagica = resistenciaMagica;
    }

    public int getAgilidade() {
        return agilidade;
    }

    public void setAgilidade(int agilidade) {
        this.agilidade = agilidade;
    }
    
    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }
    
    public int getIdPadrao() {
        return idPadrao;
    }

    public void setIdPadrao(int idPadrao) {
        this.idPadrao = idPadrao;
    }

    public List<CLArma> getListaArmas() {
        return listaArmas;
    }
    
    public CLArma getArma(int indexArma){
        return this.listaArmas.get(indexArma);
    }
    
    public void setListaArmas(CLArma arma) {
        this.listaArmas.add(arma);
    }
    
    public int countListaArmas() {
        return this.listaArmas.size();
    }

    public List<CLMagiaCura> getListaCura() {
        return listaCura;
    }
    
    public CLMagiaCura getMagiaCura(int indexMagiaCura){
        return this.listaCura.get(indexMagiaCura);
    }
    
    public void setListaCura(CLMagiaCura Cura) {
        this.listaCura.add(Cura);
    }
    
    public int countListaCura() {
        return this.listaCura.size();
    }
    
    public boolean isListaCura(){
        if(this.listaCura.isEmpty()){
            return false;
        }else{
            return true;
        }
    }
    
    public List<CLMagiaDano> getListaDano() {
        return listaDano;
    }
    
    public CLMagiaDano getMagiaDano(int indexMagiaDano){
        return this.listaDano.get(indexMagiaDano);
    }
    
    public void setListaDano(CLMagiaDano Dano) {
        this.listaDano.add(Dano);
    }
    
    public int countListaDano() {
        return this.listaDano.size();
    }
    
    public boolean isListaDano(){
        if(this.listaDano.isEmpty()){
            return false;
        }else{
            return true;
        }
    }
}


