package Classes;

public class CLArma  {


    private int id;
    private String nome;
    private int dano;
   
    public CLArma () {
    }
    
    public int getId() {
        return this.id;
    }
    
    public void setId(int id){
        this.id = id;
    }
    
    public String getNome() {
        return this.nome;
    }
    
    public void setNome(String nome){
        this.nome = nome;
    }
    
    public int getDano() {
        return this.dano;
    }
    
    public void setDano(int dano) {
        if(dano >= this.dano)
            this.dano = dano;
    }

}


