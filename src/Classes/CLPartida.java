package Classes;

import java.util.ArrayList;
import java.util.List;

public class CLPartida {

     private int id;
     private int gastoMana;
     private int indexJogadorAtivo;
     private List<CLPartidaJogador> listaJogadores;
     
    public CLPartida () {
        this.listaJogadores = new ArrayList<>();
        this.indexJogadorAtivo = 0;
    }
   
    public int getId() {
        return this.id;
    }
    
    public void setId(int id){
        this.id = id;
    }
    
    public int getGastoMana() {
        return this.gastoMana;
    }
    
    public int countJogadores() {
        return this.listaJogadores.size();
    }
    
    public int getIndexJogadorAtivo() {
        return this.indexJogadorAtivo;
    }
    
    public void setIndexJogadorAtivo(int index) {
        this.indexJogadorAtivo = index;
    }
    
    public List<CLPartidaJogador> getListaJogadores() {
        return listaJogadores;
    }
    
    public CLPartidaJogador getJogador(int index){
        return this.listaJogadores.get(index);
    }

    public void setListaJogadores(CLPartidaJogador jogador) {
        this.listaJogadores.add(jogador);
    }
    
    public void removeJogador(int index){
        this.listaJogadores.remove(index);
    }
    
    /*public void setAtualizaJogador(int index, CLPartidaJogador jogador){
        this.listaJogadores.set(index, jogador);
    }*/

}


