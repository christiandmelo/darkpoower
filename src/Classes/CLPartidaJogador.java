
package Classes;

/**
 *
 * 
 */
public class CLPartidaJogador extends CLJogador{
    private int IndexPersonagemAtivo;
    private int NumeroTurno;
    //1 para Arma e 2 para Magia Dano e 3 para Magia Cura
    private int ArmaOuMagiaAtiva;
    private int IndexArmaAtiva;
    private int IndexMagiaDanoAtiva;
    private int IndexMagiaCuraAtiva;

    public CLPartidaJogador() {
        this.IndexPersonagemAtivo = 0;
        this.IndexArmaAtiva = 0;
        this.ArmaOuMagiaAtiva = 1;
    }

    public int getIndexPersonagemAtivo() {
        return IndexPersonagemAtivo;
    }

    public void setIndexPersonagemAtivo(int IndexPersonagemAtivo) {
        this.IndexPersonagemAtivo = IndexPersonagemAtivo;
    }

    public int getNumeroTurno() {
        return NumeroTurno;
    }

    public void setNumeroTurno(int NumeroTurno) {
        this.NumeroTurno = NumeroTurno;
    }

    public int getArmaOuMagiaAtiva() {
        return ArmaOuMagiaAtiva;
    }

    public void setArmaOuMagiaAtiva(int ArmaOuMagiaAtiva) {
        this.ArmaOuMagiaAtiva = ArmaOuMagiaAtiva;
    }

    public int getIndexArmaAtiva() {
        return IndexArmaAtiva;
    }

    public void setIndexArmaAtiva(int IndexArmaAtiva) {
        this.IndexArmaAtiva = IndexArmaAtiva;
    }

    public int getIndexMagiaDanoAtiva() {
        return IndexMagiaDanoAtiva;
    }

    public void setIndexMagiaDanoAtiva(int IndexMagiaAtiva) {
        this.IndexMagiaDanoAtiva = IndexMagiaAtiva;
    }
    
    public int getIndexMagiaCuraAtiva() {
        return IndexMagiaCuraAtiva;
    }

    public void setIndexMagiaCuraAtiva(int IndexMagiaAtiva) {
        this.IndexMagiaCuraAtiva = IndexMagiaAtiva;
    }
}
