package View;

import Classes.CLArma;
import Classes.CLMagiaCura;
import Classes.CLMagiaDano;
import Classes.CLPartida;
import Classes.CLPartidaJogador;
import Classes.CLPersonagem;
import java.util.ArrayList;
import javax.swing.JOptionPane;


public class VIPartida extends javax.swing.JInternalFrame {
    int JogadoresRestantesRodada = 0;
    javax.swing.JDesktopPane jDesktopPane1;

    CLPartida Clpartida = new CLPartida();
    CLPartidaJogador jogadorPrincipal = new CLPartidaJogador();
    CLPersonagem personagemPrincipal = new CLPersonagem();
    CLArma armaAtiva = new CLArma();
    CLMagiaDano magiaDanoAtiva = new CLMagiaDano();
    CLMagiaCura magiaCuraAtiva = new CLMagiaCura();
    
    VIPartida() { }
    
    /**
     * Creates new form VIPartida
     * @param partida
     * @param jDesktopPane1
     */
    public VIPartida(CLPartida partida , javax.swing.JDesktopPane jDesktopPane1) {
        /*try {
            UIManager.setLookAndFeel(UIManager.getCrossPlatformLookAndFeelClassName());
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | UnsupportedLookAndFeelException ex) {
            Logger.getLogger(VIPartida.class.getName()).log(Level.SEVERE, null, ex);
        }*/
        initComponents();
        this.Clpartida = partida;
        this.jDesktopPane1 = jDesktopPane1;
        
        //carrega o jogador princiapal;
        CarregarClassesJogadorPrincipal();
        CarregarCabecalhoJogadorPrincipal();
        CarregarJogadorPrincipal();
        
        //carrega os metodos iniciais
        ListarJogadores();
        
    }
    
    
    private void CarregarClassesJogadorPrincipal(){
        if(this.Clpartida.countJogadores() > 1){
            this.jogadorPrincipal = this.Clpartida.getJogador(this.Clpartida.getIndexJogadorAtivo());
            this.personagemPrincipal = this.jogadorPrincipal.getPersonagem(this.jogadorPrincipal.getIndexPersonagemAtivo());
            this.armaAtiva = this.personagemPrincipal.getArma(this.jogadorPrincipal.getIndexArmaAtiva());
            if(this.personagemPrincipal.isListaCura()){
                this.magiaCuraAtiva = this.personagemPrincipal.getMagiaCura(this.jogadorPrincipal.getIndexMagiaCuraAtiva());
            }
            if(this.personagemPrincipal.isListaDano()){
                this.magiaDanoAtiva = this.personagemPrincipal.getMagiaDano(this.jogadorPrincipal.getIndexMagiaDanoAtiva());
            }
        }else{
            JOptionPane.showMessageDialog(null,"Não existe jogadores suficientes para continuar a partida!");
            this.dispose();
        }
    }
    
    private void CarregarCabecalhoJogadorPrincipal(){
        if(this.jogadorPrincipal.countPersonagens() > 1){
            if(this.jogadorPrincipal.getIndexPersonagemAtivo() == 0){
                CLPersonagem personagemSecundario = this.jogadorPrincipal.getPersonagem(1);
                btnPersonagem1.setText(this.personagemPrincipal.getNome());
                btnPersonagem2.setText(personagemSecundario.getNome());
            }else{
                CLPersonagem personagemSecundario = this.jogadorPrincipal.getPersonagem(0);
                btnPersonagem2.setText(this.personagemPrincipal.getNome());
                btnPersonagem1.setText(personagemSecundario.getNome());
            }
            btnPersonagem2.setEnabled(true);
        }else{
            btnPersonagem1.setText(this.personagemPrincipal.getNome());
            btnPersonagem2.setEnabled(false);
            btnPersonagem2.setText("Morto!!!");
        }
        txtNomeJogadorPrincipal.setText(jogadorPrincipal.getNome());
    }
    
    
    private void CarregarJogadorPrincipal(){
        if(this.jogadorPrincipal.getIndexPersonagemAtivo() == 1){
            btnPersonagem1.setSelected(false);
            btnPersonagem2.setSelected(true);
        }else{
            btnPersonagem1.setSelected(true);
            btnPersonagem2.setSelected(false);
        }
        
        progressPontosDeVida.setMaximum(this.personagemPrincipal.getPontosDeVida());
        progressPontosDeVida.setValue(this.personagemPrincipal.getPontosDeVidaPerdidos());
        progressPontosDeVida.setString(this.personagemPrincipal.getPontosDeVidaPerdidos()+" / "+this.personagemPrincipal.getPontosDeVida());
        progressPontosDeMana.setMaximum(this.personagemPrincipal.getPontosDeMana());
        progressPontosDeMana.setValue(this.personagemPrincipal.getPontosDeManaPerdidos());
        progressPontosDeMana.setString(this.personagemPrincipal.getPontosDeManaPerdidos()+" / "+this.personagemPrincipal.getPontosDeMana());
        progressAtaqueEspecial.setValue(this.personagemPrincipal.getAtaqueEspecial());
        progressAtaqueEspecial.setString(this.personagemPrincipal.getAtaqueEspecial()+ " / 10");
        if(this.personagemPrincipal.getAtaqueEspecial() >= 10){
            btnUsarAtaqueEspecial.setEnabled(true);
        }else{
            btnUsarAtaqueEspecial.setEnabled(false);
        }
        
        
        txtForcaFisica.setText(Integer.toString(this.personagemPrincipal.getForcaFisica()));
        txtForcaMagia.setText(Integer.toString(this.personagemPrincipal.getForcaFisica()));
        txtResistenciaFisica.setText(Integer.toString(this.personagemPrincipal.getResistenciaFisica()));
        txtResistenciaMagica.setText(Integer.toString(this.personagemPrincipal.getResistenciaMagica()));
        txtAgilidade.setText(Integer.toString(this.personagemPrincipal.getAgilidade()));
        
        TrocarArma();
        txtNomeArma.setText(this.armaAtiva.getNome());
        txtDanoArma.setText(Integer.toString(this.armaAtiva.getDano()));
        if(this.personagemPrincipal.countListaArmas() <= 1){
            btnTrocarArma.setEnabled(false);
        }else{
            btnTrocarArma.setEnabled(true);
        }
        
        if(this.personagemPrincipal.isListaDano()){
            txtNomeMagiaDano.setText(this.magiaDanoAtiva.getNome());
            txtDanoMagiaDano.setText(Integer.toString(this.magiaDanoAtiva.getDano()));
            panelMagiaDano.setVisible(true);
            if(this.personagemPrincipal.countListaDano() <= 1){
                btnTrocarMagiaDano.setEnabled(false);
            }else{
                btnTrocarMagiaDano.setEnabled(true);
            }
        }else{
            panelMagiaDano.setVisible(false);
        }
        
        if(this.personagemPrincipal.isListaCura()){
            txtNomeMagiaCura.setText(this.magiaCuraAtiva.getNome());
            txtCura.setText(Integer.toString(this.magiaCuraAtiva.getCura()));
            panelMagiaCura.setVisible(true);
            if(this.personagemPrincipal.countListaCura() <= 1){
                btnTrocarMagiaCura.setEnabled(false);
            }else{
                btnTrocarMagiaCura.setEnabled(true);
            }
        }else{
            panelMagiaCura.setVisible(false);
        }
    }
        
    private void TrocarArma(){
        switch(this.jogadorPrincipal.getArmaOuMagiaAtiva()){
            case 1:
                btnUsarArma.setSelected(true);
                btnUsarMagiaDano.setSelected(false);
                btnUsarMagiaCura.setSelected(false);
                break;
            case 2:
                btnUsarArma.setSelected(false);
                btnUsarMagiaDano.setSelected(true);
                btnUsarMagiaCura.setSelected(false);
                break;
        }
    }
    
    
    public void TrocarJogador(){
        this.JogadoresRestantesRodada = this.JogadoresRestantesRodada + 1;
        if(this.JogadoresRestantesRodada == this.Clpartida.countJogadores() || this.JogadoresRestantesRodada > this.Clpartida.countJogadores()){
            for(CLPartidaJogador pj : this.Clpartida.getListaJogadores()){
                for(CLPersonagem cp : pj.getListaPersonagens()){
                    cp.setAtaqueEspecialPositivo();
                    if(cp.getId() != this.personagemPrincipal.getId() && this.jogadorPrincipal.getArmaOuMagiaAtiva() == 1){
                        cp.setPontosDeManaPerdidosPositivo(10);
                    }
                }
            }
            this.JogadoresRestantesRodada = 0;
        }
        this.Clpartida.setIndexJogadorAtivo(this.JogadoresRestantesRodada);
        //carrega o jogador princiapal;
        CarregarClassesJogadorPrincipal();
        CarregarCabecalhoJogadorPrincipal();
        CarregarJogadorPrincipal();
        
        //carrega os metodos iniciais
        ListarJogadores();
        
    }
    
    
    public void RealizarAtaque(String NomeJogadorAtacado){
        int indexJ = 0;
        boolean ManaValidada = true;
        if(this.jogadorPrincipal.getArmaOuMagiaAtiva() == 2){
            if(this.personagemPrincipal.getPontosDeManaPerdidos() < this.magiaDanoAtiva.getGastoDeMana()){
                ManaValidada = false;
                JOptionPane.showMessageDialog(null,"Seu personagem não tem pontos de mana suficientes para atacar com magia!");
            }
        }
        if(ManaValidada){
            for(CLPartidaJogador jogadorAtacado : this.Clpartida.getListaJogadores()){
                int indexP = 0;
                if(jogadorAtacado.getNome().equals(NomeJogadorAtacado)){
                    for(CLPersonagem pe : jogadorAtacado.getListaPersonagens()){
                        if(indexP == jogadorAtacado.getIndexPersonagemAtivo()){
                            int defesa = 0;
                            int dano = 0;
                            if(this.jogadorPrincipal.getArmaOuMagiaAtiva() == 1){
                                if(this.btnUsarAtaqueEspecial.isSelected()){
                                    this.personagemPrincipal.setAtaqueEspecialNegativo();
                                    int novoDano = this.armaAtiva.getDano()+this.personagemPrincipal.getForcaFisica();
                                    if(pe.getTipo().equals("humano")){
                                        dano = (int) (novoDano*0.5);
                                    }else{
                                        dano = (int) (novoDano*0.2);
                                    }
                                }else{
                                    dano = this.armaAtiva.getDano()+this.personagemPrincipal.getForcaFisica();
                                }
                                defesa = pe.getResistenciaFisica()+pe.getAgilidade();
                            }else{
                                if(this.btnUsarAtaqueEspecial.isSelected()){
                                    this.personagemPrincipal.setAtaqueEspecialNegativo();
                                    int novoDano = magiaDanoAtiva.getDano()+this.personagemPrincipal.getForcaMagica();
                                    if(pe.getTipo().equals("inumano")){
                                        dano = (int) (novoDano*0.5);
                                    }else{
                                        dano = (int) (novoDano*0.2);
                                    }
                                }else{
                                    dano = magiaDanoAtiva.getDano()+this.personagemPrincipal.getForcaMagica();
                                }
                                this.personagemPrincipal.setPontosDeManaPerdidosNegativos(this.magiaDanoAtiva.getGastoDeMana());
                                defesa = pe.getResistenciaMagica()+pe.getAgilidade();
                            }
                            int pontos = dano - defesa;
                            if(!pe.setPontosDeVidaPerdidosNegativo(pontos)){
                                jogadorAtacado.removePersonagem(indexP);
                                if(jogadorAtacado.countPersonagens() == 0){
                                    if(this.JogadoresRestantesRodada > indexJ){
                                        this.JogadoresRestantesRodada = this.JogadoresRestantesRodada - 1;
                                    }
                                    Clpartida.removeJogador(indexJ);
                                }
                            }
                            if(Clpartida.countJogadores() <= 1){
                                JOptionPane.showMessageDialog(null,"Parabéns o jogagor "+jogadorPrincipal.getNome()+ " foi o vencedor!!!");
                                this.dispose();
                            }else{
                                JOptionPane.showMessageDialog(null,"Ataque realizado com sucesso, Agora é a vez de outro jogador!!");
                                TrocarJogador();
                            }
                        }
                        indexP = indexP + 1;
                    }
                }
                indexJ = indexJ + 1;
            }
            
        }
    }
    
    
    private void ListarJogadores(){
        btnJogador1.setText("Jogador 1");
        btnJogador1.setEnabled(false);
        btnJogador2.setText("Jogador 2");
        btnJogador2.setEnabled(false);
        btnJogador3.setText("Jogador 3");
        btnJogador3.setEnabled(false);
        btnJogador4.setText("Jogador 4");
        btnJogador4.setEnabled(false);
        btnJogador5.setText("Jogador 5");
        btnJogador5.setEnabled(false);
        btnJogador6.setText("Jogador 6");
        btnJogador6.setEnabled(false);
        btnJogador7.setText("Jogador 7");
        btnJogador7.setEnabled(false);
        btnJogador8.setText("Jogador 8");
        btnJogador8.setEnabled(false);
        btnJogador9.setText("Jogador 9");
        btnJogador9.setEnabled(false);
        if(this.Clpartida.countJogadores() > 1){
            int contador = 0;
            for(CLPartidaJogador cpj : this.Clpartida.getListaJogadores()){
                if(!this.jogadorPrincipal.getNome().equals(cpj.getNome())){
                    switch(contador){
                        case 0:
                            btnJogador1.setText(cpj.getNome());
                            btnJogador1.setEnabled(true);
                            break;
                        case 1:
                            btnJogador2.setText(cpj.getNome());
                            btnJogador2.setEnabled(true);
                            break;
                        case 2:
                            btnJogador3.setText(cpj.getNome());
                            btnJogador3.setEnabled(true);
                            break;
                        case 3:
                            btnJogador4.setText(cpj.getNome());
                            btnJogador4.setEnabled(true);
                            break;
                        case 4:
                            btnJogador5.setText(cpj.getNome());
                            btnJogador5.setEnabled(true);
                            break;
                        case 5:
                            btnJogador6.setText(cpj.getNome());
                            btnJogador6.setEnabled(true);
                            break;
                        case 6:
                            btnJogador7.setText(cpj.getNome());
                            btnJogador7.setEnabled(true);
                            break;
                        case 7:
                            btnJogador8.setText(cpj.getNome());
                            btnJogador8.setEnabled(true);
                            break;
                        case 8:
                            btnJogador9.setText(cpj.getNome());
                            btnJogador9.setEnabled(true);
                            break;
                    }
                contador = contador + 1;
                }
            }
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        btnJogador9 = new javax.swing.JButton();
        btnJogador1 = new javax.swing.JButton();
        btnJogador2 = new javax.swing.JButton();
        btnJogador3 = new javax.swing.JButton();
        btnJogador5 = new javax.swing.JButton();
        btnJogador4 = new javax.swing.JButton();
        btnJogador6 = new javax.swing.JButton();
        btnJogador7 = new javax.swing.JButton();
        btnJogador8 = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        txtForcaFisica = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        txtResistenciaFisica = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        jLabel13 = new javax.swing.JLabel();
        txtResistenciaMagica = new javax.swing.JLabel();
        txtAgilidade = new javax.swing.JLabel();
        jPanel3 = new javax.swing.JPanel();
        jLabel12 = new javax.swing.JLabel();
        txtNomeArma = new javax.swing.JLabel();
        btnTrocarArma = new javax.swing.JButton();
        jLabel17 = new javax.swing.JLabel();
        txtDanoArma = new javax.swing.JLabel();
        btnUsarArma = new javax.swing.JToggleButton();
        panelMagiaDano = new javax.swing.JPanel();
        jLabel19 = new javax.swing.JLabel();
        txtNomeMagiaDano = new javax.swing.JLabel();
        btnTrocarMagiaDano = new javax.swing.JButton();
        jLabel21 = new javax.swing.JLabel();
        txtDanoMagiaDano = new javax.swing.JLabel();
        btnUsarMagiaDano = new javax.swing.JToggleButton();
        progressPontosDeVida = new javax.swing.JProgressBar();
        progressPontosDeMana = new javax.swing.JProgressBar();
        jLabel27 = new javax.swing.JLabel();
        jLabel28 = new javax.swing.JLabel();
        txtForcaMagia = new javax.swing.JLabel();
        progressAtaqueEspecial = new javax.swing.JProgressBar();
        btnUsarAtaqueEspecial = new javax.swing.JButton();
        btnPersonagem2 = new javax.swing.JToggleButton();
        btnPersonagem1 = new javax.swing.JToggleButton();
        panelMagiaCura = new javax.swing.JPanel();
        jLabel20 = new javax.swing.JLabel();
        txtNomeMagiaCura = new javax.swing.JLabel();
        btnTrocarMagiaCura = new javax.swing.JButton();
        jLabel24 = new javax.swing.JLabel();
        txtCura = new javax.swing.JLabel();
        btnUsarMagiaCura = new javax.swing.JButton();
        jLabel25 = new javax.swing.JLabel();
        txtNomeJogadorPrincipal = new javax.swing.JLabel();

        setClosable(true);
        setIconifiable(true);
        setTitle("Partida");

        jPanel1.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

        btnJogador9.setText("Jogador 9");
        btnJogador9.setEnabled(false);
        btnJogador9.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnJogador9ActionPerformed(evt);
            }
        });

        btnJogador1.setText("Jogador 1");
        btnJogador1.setEnabled(false);
        btnJogador1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnJogador1ActionPerformed(evt);
            }
        });

        btnJogador2.setText("Jogador 2");
        btnJogador2.setEnabled(false);
        btnJogador2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnJogador2ActionPerformed(evt);
            }
        });

        btnJogador3.setText("Jogador 3");
        btnJogador3.setEnabled(false);
        btnJogador3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnJogador3ActionPerformed(evt);
            }
        });

        btnJogador5.setText("Jogador 5");
        btnJogador5.setEnabled(false);
        btnJogador5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnJogador5ActionPerformed(evt);
            }
        });

        btnJogador4.setText("Jogadro 4");
        btnJogador4.setEnabled(false);
        btnJogador4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnJogador4ActionPerformed(evt);
            }
        });

        btnJogador6.setText("Jogador 6");
        btnJogador6.setEnabled(false);
        btnJogador6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnJogador6ActionPerformed(evt);
            }
        });

        btnJogador7.setText("Jogador 7");
        btnJogador7.setEnabled(false);
        btnJogador7.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnJogador7ActionPerformed(evt);
            }
        });

        btnJogador8.setText("Jogadro 8");
        btnJogador8.setEnabled(false);
        btnJogador8.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnJogador8ActionPerformed(evt);
            }
        });

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel1.setText("Clique no jogador para ataca-lo");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btnJogador2, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btnJogador1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btnJogador3, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btnJogador4, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btnJogador5, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btnJogador6, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btnJogador7, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btnJogador8, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btnJogador9, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(109, 109, 109)
                .addComponent(jLabel1)
                .addContainerGap(108, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel1)
                .addGap(36, 36, 36)
                .addComponent(btnJogador1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnJogador2)
                .addGap(4, 4, 4)
                .addComponent(btnJogador3)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnJogador4)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnJogador5)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnJogador6)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnJogador7)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnJogador8)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnJogador9)
                .addGap(40, 40, 40))
        );

        jPanel2.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

        jLabel4.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel4.setText("Pontos de Mana");

        jLabel5.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel5.setText("Força Física");

        txtForcaFisica.setText("0");

        jLabel8.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel8.setText("Força Mágica");

        jLabel9.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel9.setText("Resistencia Física");

        txtResistenciaFisica.setText("0");

        jLabel11.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel11.setText("Resistencia Mágica");

        jLabel13.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel13.setText("Agilidade");

        txtResistenciaMagica.setText("0");

        txtAgilidade.setText("0");

        jPanel3.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

        jLabel12.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel12.setText("Arma");

        txtNomeArma.setText("arma");

        btnTrocarArma.setText("Trocar");
        btnTrocarArma.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnTrocarArmaActionPerformed(evt);
            }
        });

        jLabel17.setText("Dano:");

        txtDanoArma.setText("0");

        btnUsarArma.setText("Usar Arma");
        btnUsarArma.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnUsarArmaActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                        .addComponent(btnUsarArma)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btnTrocarArma)
                        .addGap(21, 21, 21))
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel3Layout.createSequentialGroup()
                                .addComponent(jLabel17)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txtDanoArma))
                            .addComponent(jLabel12)
                            .addComponent(txtNomeArma, javax.swing.GroupLayout.PREFERRED_SIZE, 146, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel12)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtNomeArma)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel17)
                    .addComponent(txtDanoArma))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnTrocarArma)
                    .addComponent(btnUsarArma))
                .addContainerGap())
        );

        panelMagiaDano.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

        jLabel19.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel19.setText("Mágia Dano");

        txtNomeMagiaDano.setText("magia");

        btnTrocarMagiaDano.setText("Trocar");
        btnTrocarMagiaDano.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnTrocarMagiaDanoActionPerformed(evt);
            }
        });

        jLabel21.setText("Dano:");

        txtDanoMagiaDano.setText("0");

        btnUsarMagiaDano.setText("Usar Mágia");
        btnUsarMagiaDano.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnUsarMagiaDanoActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout panelMagiaDanoLayout = new javax.swing.GroupLayout(panelMagiaDano);
        panelMagiaDano.setLayout(panelMagiaDanoLayout);
        panelMagiaDanoLayout.setHorizontalGroup(
            panelMagiaDanoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelMagiaDanoLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panelMagiaDanoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelMagiaDanoLayout.createSequentialGroup()
                        .addComponent(btnUsarMagiaDano)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btnTrocarMagiaDano)
                        .addGap(21, 21, 21))
                    .addGroup(panelMagiaDanoLayout.createSequentialGroup()
                        .addGroup(panelMagiaDanoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtNomeMagiaDano)
                            .addGroup(panelMagiaDanoLayout.createSequentialGroup()
                                .addComponent(jLabel21)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txtDanoMagiaDano))
                            .addComponent(jLabel19))
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
        );
        panelMagiaDanoLayout.setVerticalGroup(
            panelMagiaDanoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelMagiaDanoLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel19)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtNomeMagiaDano)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panelMagiaDanoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel21)
                    .addComponent(txtDanoMagiaDano))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(panelMagiaDanoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnTrocarMagiaDano)
                    .addComponent(btnUsarMagiaDano))
                .addContainerGap())
        );

        progressPontosDeVida.setMaximum(4000);
        progressPontosDeVida.setValue(200);
        progressPontosDeVida.setStringPainted(true);

        progressPontosDeMana.setMaximum(10);
        progressPontosDeMana.setValue(2);
        progressPontosDeMana.setStringPainted(true);

        jLabel27.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel27.setText("Pontos de Vida");

        jLabel28.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel28.setText("Ataque Especial");

        txtForcaMagia.setText("0");

        progressAtaqueEspecial.setMaximum(10);
        progressAtaqueEspecial.setValue(3);
        progressAtaqueEspecial.setStringPainted(true);

        btnUsarAtaqueEspecial.setText("Usar Ataque");
        btnUsarAtaqueEspecial.setEnabled(false);
        btnUsarAtaqueEspecial.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnUsarAtaqueEspecialActionPerformed(evt);
            }
        });

        btnPersonagem2.setText("Personagem 2");
        btnPersonagem2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnPersonagem2ActionPerformed(evt);
            }
        });

        btnPersonagem1.setSelected(true);
        btnPersonagem1.setText("Personagem 1");
        btnPersonagem1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnPersonagem1ActionPerformed(evt);
            }
        });

        panelMagiaCura.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

        jLabel20.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel20.setText("Mágia Cura");

        txtNomeMagiaCura.setText("magia");

        btnTrocarMagiaCura.setText("Trocar");
        btnTrocarMagiaCura.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnTrocarMagiaCuraActionPerformed(evt);
            }
        });

        jLabel24.setText("Cura:");

        txtCura.setText("0");

        btnUsarMagiaCura.setText("Usar Cura");
        btnUsarMagiaCura.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnUsarMagiaCuraActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout panelMagiaCuraLayout = new javax.swing.GroupLayout(panelMagiaCura);
        panelMagiaCura.setLayout(panelMagiaCuraLayout);
        panelMagiaCuraLayout.setHorizontalGroup(
            panelMagiaCuraLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelMagiaCuraLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panelMagiaCuraLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panelMagiaCuraLayout.createSequentialGroup()
                        .addComponent(btnUsarMagiaCura, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnTrocarMagiaCura)
                        .addGap(21, 21, 21))
                    .addGroup(panelMagiaCuraLayout.createSequentialGroup()
                        .addGroup(panelMagiaCuraLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtNomeMagiaCura)
                            .addGroup(panelMagiaCuraLayout.createSequentialGroup()
                                .addComponent(jLabel24)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txtCura))
                            .addComponent(jLabel20))
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
        );
        panelMagiaCuraLayout.setVerticalGroup(
            panelMagiaCuraLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelMagiaCuraLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel20)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtNomeMagiaCura)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(panelMagiaCuraLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel24)
                    .addComponent(txtCura))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(panelMagiaCuraLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnTrocarMagiaCura)
                    .addComponent(btnUsarMagiaCura))
                .addContainerGap())
        );

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(progressPontosDeMana, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addComponent(progressAtaqueEspecial, javax.swing.GroupLayout.PREFERRED_SIZE, 159, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btnUsarAtaqueEspecial, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(jLabel4, javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel5, javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(txtForcaFisica, javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel8, javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel13, javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel9, javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(txtResistenciaFisica, javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel11, javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(txtResistenciaMagica, javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(txtAgilidade, javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel27, javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel28, javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(txtForcaMagia, javax.swing.GroupLayout.Alignment.LEADING))
                                .addGap(0, 0, Short.MAX_VALUE))
                            .addComponent(progressPontosDeVida, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGap(30, 30, 30)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(panelMagiaDano, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(panelMagiaCura, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(btnPersonagem1, javax.swing.GroupLayout.PREFERRED_SIZE, 238, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnPersonagem2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(2, 2, 2)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnPersonagem1)
                    .addComponent(btnPersonagem2))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(jLabel27)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(progressPontosDeVida, javax.swing.GroupLayout.PREFERRED_SIZE, 19, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel4)
                        .addGap(1, 1, 1)
                        .addComponent(progressPontosDeMana, javax.swing.GroupLayout.PREFERRED_SIZE, 19, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel28)
                        .addGap(3, 3, 3)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(progressAtaqueEspecial, javax.swing.GroupLayout.PREFERRED_SIZE, 19, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btnUsarAtaqueEspecial, javax.swing.GroupLayout.PREFERRED_SIZE, 19, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel5)
                        .addGap(1, 1, 1)
                        .addComponent(txtForcaFisica)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel8)
                        .addGap(1, 1, 1)
                        .addComponent(txtForcaMagia)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel9)
                        .addGap(1, 1, 1)
                        .addComponent(txtResistenciaFisica)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel11)
                        .addGap(1, 1, 1)
                        .addComponent(txtResistenciaMagica)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel13)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtAgilidade)
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(panelMagiaDano, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(panelMagiaCura, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 10, Short.MAX_VALUE))))
        );

        jLabel25.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel25.setText("Jogador:");

        txtNomeJogadorPrincipal.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        txtNomeJogadorPrincipal.setText("Jogador 1");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(21, 21, 21)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel25)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtNomeJogadorPrincipal, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(53, 53, 53)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(41, 41, 41)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel25)
                    .addComponent(txtNomeJogadorPrincipal))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnJogador1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnJogador1ActionPerformed
        this.RealizarAtaque(this.btnJogador1.getText());
    }//GEN-LAST:event_btnJogador1ActionPerformed

    private void btnTrocarArmaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnTrocarArmaActionPerformed
        VIPartidaTrocarArma tr = new VIPartidaTrocarArma(this,(ArrayList<CLArma>) this.personagemPrincipal.getListaArmas(), this.armaAtiva, this.jogadorPrincipal);
        this.jDesktopPane1.add(tr);
        tr.setVisible(true);
        tr.setLocation(300, 100);
    }//GEN-LAST:event_btnTrocarArmaActionPerformed

    private void btnTrocarMagiaDanoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnTrocarMagiaDanoActionPerformed
        VIPartidaTrocarMagiaDano tr = new VIPartidaTrocarMagiaDano(this,(ArrayList<CLMagiaDano>) this.personagemPrincipal.getListaDano(), this.magiaDanoAtiva, this.jogadorPrincipal);
        this.jDesktopPane1.add(tr);
        tr.setVisible(true);
        tr.setLocation(300, 100);
    }//GEN-LAST:event_btnTrocarMagiaDanoActionPerformed

    private void btnUsarArmaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnUsarArmaActionPerformed
        this.jogadorPrincipal.setArmaOuMagiaAtiva(1);
        TrocarArma();
    }//GEN-LAST:event_btnUsarArmaActionPerformed

    private void btnUsarAtaqueEspecialActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnUsarAtaqueEspecialActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_btnUsarAtaqueEspecialActionPerformed

    private void btnTrocarMagiaCuraActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnTrocarMagiaCuraActionPerformed
        VIPartidaTrocarMagiaCura tr = new VIPartidaTrocarMagiaCura(this,(ArrayList<CLMagiaCura>) this.personagemPrincipal.getListaCura(), this.magiaCuraAtiva, this.jogadorPrincipal);
        this.jDesktopPane1.add(tr);
        tr.setVisible(true);
        tr.setLocation(300, 100);
    }//GEN-LAST:event_btnTrocarMagiaCuraActionPerformed

    private void btnPersonagem2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnPersonagem2ActionPerformed
        this.jogadorPrincipal.setIndexPersonagemAtivo(1);
        CarregarClassesJogadorPrincipal();
        CarregarJogadorPrincipal();
    }//GEN-LAST:event_btnPersonagem2ActionPerformed

    private void btnPersonagem1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnPersonagem1ActionPerformed
        this.jogadorPrincipal.setIndexPersonagemAtivo(0);
        CarregarClassesJogadorPrincipal();
        CarregarJogadorPrincipal();
    }//GEN-LAST:event_btnPersonagem1ActionPerformed

    private void btnUsarMagiaDanoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnUsarMagiaDanoActionPerformed
        this.jogadorPrincipal.setArmaOuMagiaAtiva(2);
        TrocarArma();
    }//GEN-LAST:event_btnUsarMagiaDanoActionPerformed

    private void btnUsarMagiaCuraActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnUsarMagiaCuraActionPerformed
        if(this.jogadorPrincipal.getArmaOuMagiaAtiva() == 3){
            if(this.personagemPrincipal.getPontosDeManaPerdidos() < this.magiaCuraAtiva.getGastoDeMana()){
                JOptionPane.showMessageDialog(null,"Seu personagem não tem pontos de mana suficientes para atacar com magia!");
            }
        }else{
            this.personagemPrincipal.setPontosDeManaPerdidosNegativos(this.magiaCuraAtiva.getGastoDeMana());
            this.personagemPrincipal.setPontosDeVidaPerdidosPositivo(this.magiaCuraAtiva.getCura());
            JOptionPane.showMessageDialog(null,"Cura realizada com sucesso! Agora é a vez de outro jogador.");
            TrocarJogador();
        }
    }//GEN-LAST:event_btnUsarMagiaCuraActionPerformed

    private void btnJogador2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnJogador2ActionPerformed
        this.RealizarAtaque(this.btnJogador2.getText());
    }//GEN-LAST:event_btnJogador2ActionPerformed

    private void btnJogador3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnJogador3ActionPerformed
        this.RealizarAtaque(this.btnJogador3.getText());
    }//GEN-LAST:event_btnJogador3ActionPerformed

    private void btnJogador4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnJogador4ActionPerformed
        this.RealizarAtaque(this.btnJogador4.getText());
    }//GEN-LAST:event_btnJogador4ActionPerformed

    private void btnJogador5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnJogador5ActionPerformed
        this.RealizarAtaque(this.btnJogador5.getText());
    }//GEN-LAST:event_btnJogador5ActionPerformed

    private void btnJogador6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnJogador6ActionPerformed
        this.RealizarAtaque(this.btnJogador6.getText());
    }//GEN-LAST:event_btnJogador6ActionPerformed

    private void btnJogador7ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnJogador7ActionPerformed
        this.RealizarAtaque(this.btnJogador7.getText());
    }//GEN-LAST:event_btnJogador7ActionPerformed

    private void btnJogador8ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnJogador8ActionPerformed
        this.RealizarAtaque(this.btnJogador8.getText());
    }//GEN-LAST:event_btnJogador8ActionPerformed

    private void btnJogador9ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnJogador9ActionPerformed
        this.RealizarAtaque(this.btnJogador9.getText());
    }//GEN-LAST:event_btnJogador9ActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnJogador1;
    private javax.swing.JButton btnJogador2;
    private javax.swing.JButton btnJogador3;
    private javax.swing.JButton btnJogador4;
    private javax.swing.JButton btnJogador5;
    private javax.swing.JButton btnJogador6;
    private javax.swing.JButton btnJogador7;
    private javax.swing.JButton btnJogador8;
    private javax.swing.JButton btnJogador9;
    private javax.swing.JToggleButton btnPersonagem1;
    private javax.swing.JToggleButton btnPersonagem2;
    private javax.swing.JButton btnTrocarArma;
    private javax.swing.JButton btnTrocarMagiaCura;
    private javax.swing.JButton btnTrocarMagiaDano;
    private javax.swing.JToggleButton btnUsarArma;
    private javax.swing.JButton btnUsarAtaqueEspecial;
    private javax.swing.JButton btnUsarMagiaCura;
    private javax.swing.JToggleButton btnUsarMagiaDano;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel21;
    private javax.swing.JLabel jLabel24;
    private javax.swing.JLabel jLabel25;
    private javax.swing.JLabel jLabel27;
    private javax.swing.JLabel jLabel28;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel panelMagiaCura;
    private javax.swing.JPanel panelMagiaDano;
    private javax.swing.JProgressBar progressAtaqueEspecial;
    private javax.swing.JProgressBar progressPontosDeMana;
    private javax.swing.JProgressBar progressPontosDeVida;
    private javax.swing.JLabel txtAgilidade;
    private javax.swing.JLabel txtCura;
    private javax.swing.JLabel txtDanoArma;
    private javax.swing.JLabel txtDanoMagiaDano;
    private javax.swing.JLabel txtForcaFisica;
    private javax.swing.JLabel txtForcaMagia;
    private javax.swing.JLabel txtNomeArma;
    private javax.swing.JLabel txtNomeJogadorPrincipal;
    private javax.swing.JLabel txtNomeMagiaCura;
    private javax.swing.JLabel txtNomeMagiaDano;
    private javax.swing.JLabel txtResistenciaFisica;
    private javax.swing.JLabel txtResistenciaMagica;
    // End of variables declaration//GEN-END:variables
}
