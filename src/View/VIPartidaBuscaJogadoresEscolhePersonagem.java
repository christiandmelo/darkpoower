package View;

import Classes.CLJogador;
import Classes.CLPartida;
import Classes.CLPartidaJogador;
import Classes.CLPersonagem;
import RegraNegocio.Conversor;
import RegraNegocio.RNJogador;
import RegraNegocio.RNPersonagem;
import java.util.ArrayList;
import javax.swing.JOptionPane;

public class VIPartidaBuscaJogadoresEscolhePersonagem extends javax.swing.JInternalFrame {

    int qtdPersonagens;
    javax.swing.JLabel QtdJogadores;
    javax.swing.JTextArea JLista;
    
    CLPartida Clpartida = new CLPartida();
    CLJogador Jogador = new CLJogador();
    ArrayList<CLPersonagem> ListaPersonagens;
    Conversor conversor = new Conversor();
    RNJogador RNj = new RNJogador();
    RNPersonagem RNp = new RNPersonagem();
    
    CLPersonagem guerreiro;
    CLPersonagem ladrao;
    CLPersonagem mago;
    CLPersonagem paladino;
    CLPersonagem animal;
    CLPersonagem troll;
    CLPersonagem dragao;
    CLPersonagem zumbi;
    
    public VIPartidaBuscaJogadoresEscolhePersonagem(CLPartida partida, CLJogador jogador, javax.swing.JTextArea listaJogadores, javax.swing.JLabel qtdjogadores) {
        initComponents();
        
        this.ListaPersonagens = new ArrayList<>();
        this.Clpartida = partida;
        this.qtdPersonagens = 0;
        this.QtdJogadores = qtdjogadores;
        this.JLista = listaJogadores;
        
        this.Jogador.setId(jogador.getId());
        this.Jogador.setNome(jogador.getNome());
        this.Jogador.setEmail(jogador.getEmail());
        AtivaPersonagensJogador((ArrayList<CLPersonagem>) jogador.getListaPersonagens());
    }
    
    public VIPartidaBuscaJogadoresEscolhePersonagem(int idJogador,CLPartida partida, javax.swing.JTextArea listaJogadores, javax.swing.JLabel qtdjogadores) {
        initComponents();
        
        this.ListaPersonagens = new ArrayList<>();
        this.Clpartida = partida;
        this.qtdPersonagens = 0;
        this.QtdJogadores = qtdjogadores;
        this.JLista = listaJogadores;
        
        Jogador = RNj.ListarJogadorPorId(idJogador);
        this.ListaPersonagens = (RNp.ListarPersonagensPorJogador(conversor.CLJogadorEmJogador(Jogador)));
        AtivaPersonagensJogador(this.ListaPersonagens);
        
    }
    
    public void FecharFrame(){
        this.dispose();
    }
    
    
    public void AtivaPersonagensJogador(ArrayList<CLPersonagem> personagens){
        txtNome.setText(Jogador.getNome());
        txtEmail.setText(Jogador.getEmail());
        for(CLPersonagem personagem : personagens){
            switch(personagem.getIdPadrao()){
                case 1:
                    this.guerreiro = personagem;
                    break;
                case 2:
                    this.ladrao = personagem;
                    break;
                case 3:
                    this.mago = personagem;
                    break;
                case 4:
                    this.paladino = personagem;
                    break;
                case 5:
                    this.animal = personagem;
                    break;
                case 6:
                    this.troll = personagem;
                    break;
                case 7:
                    this.dragao = personagem;
                    break;
                case 8:
                    this.zumbi = personagem;
                    break;
            }
        }
    }
    
    public boolean ValidaQuantidadePersonagensVinculados(){
        if(qtdPersonagens < 2){
            qtdPersonagens = qtdPersonagens + 1;
            return true;
        }else{
            JOptionPane.showMessageDialog(null,"Só pode escolher dois personagens!");
            return false;
        }
    }
    

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel2 = new javax.swing.JLabel();
        txtNome = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        txtEmail = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        btnGuerreiro = new javax.swing.JButton();
        btnLadrao = new javax.swing.JButton();
        btnMago = new javax.swing.JButton();
        btnPaladino = new javax.swing.JButton();
        btnAnimal = new javax.swing.JButton();
        btnTroll = new javax.swing.JButton();
        btnDragao = new javax.swing.JButton();
        btnZumbi = new javax.swing.JButton();
        btnFinalizar = new javax.swing.JButton();

        setClosable(true);
        setIconifiable(true);
        setMaximizable(true);
        setTitle("Personagens do jogador");

        jLabel2.setText("Nome");

        txtNome.setEnabled(false);

        jLabel3.setText("Email");

        txtEmail.setEnabled(false);

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel1.setText("Escolha os personagens para a partida");

        jPanel1.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

        btnGuerreiro.setText("Guerreiro");
        btnGuerreiro.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnGuerreiroActionPerformed(evt);
            }
        });

        btnLadrao.setText("Ladrão");
        btnLadrao.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnLadraoActionPerformed(evt);
            }
        });

        btnMago.setText("Mago");
        btnMago.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnMagoActionPerformed(evt);
            }
        });

        btnPaladino.setText("Paladino");
        btnPaladino.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnPaladinoActionPerformed(evt);
            }
        });

        btnAnimal.setText("Animal");
        btnAnimal.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAnimalActionPerformed(evt);
            }
        });

        btnTroll.setText("Troll");
        btnTroll.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnTrollActionPerformed(evt);
            }
        });

        btnDragao.setText("Dragão");
        btnDragao.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDragaoActionPerformed(evt);
            }
        });

        btnZumbi.setText("Zumbi");
        btnZumbi.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnZumbiActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(btnGuerreiro, javax.swing.GroupLayout.DEFAULT_SIZE, 94, Short.MAX_VALUE)
                    .addComponent(btnLadrao, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(52, 52, 52)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(btnMago, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btnPaladino, javax.swing.GroupLayout.DEFAULT_SIZE, 100, Short.MAX_VALUE))
                .addGap(56, 56, 56)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(btnAnimal, javax.swing.GroupLayout.DEFAULT_SIZE, 93, Short.MAX_VALUE)
                    .addComponent(btnTroll, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 52, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(btnDragao, javax.swing.GroupLayout.DEFAULT_SIZE, 95, Short.MAX_VALUE)
                    .addComponent(btnZumbi, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(31, 31, 31))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(btnDragao)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(btnZumbi))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(btnGuerreiro)
                            .addComponent(btnMago)
                            .addComponent(btnAnimal))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(btnLadrao)
                            .addComponent(btnPaladino)
                            .addComponent(btnTroll))))
                .addContainerGap(25, Short.MAX_VALUE))
        );

        btnFinalizar.setText("Finalizar");
        btnFinalizar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnFinalizarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(btnFinalizar, javax.swing.GroupLayout.PREFERRED_SIZE, 117, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(layout.createSequentialGroup()
                            .addGap(82, 82, 82)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(jLabel2)
                                .addComponent(jLabel3))
                            .addGap(10, 10, 10)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(txtNome, javax.swing.GroupLayout.PREFERRED_SIZE, 256, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(txtEmail, javax.swing.GroupLayout.PREFERRED_SIZE, 256, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel1)))
                .addContainerGap(51, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(25, 25, 25)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel2)
                        .addGap(16, 16, 16)
                        .addComponent(jLabel3))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(txtNome, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(9, 9, 9)
                        .addComponent(txtEmail, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 37, Short.MAX_VALUE)
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(btnFinalizar)
                .addGap(108, 108, 108))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnGuerreiroActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnGuerreiroActionPerformed
        if(ValidaQuantidadePersonagensVinculados()){
            btnGuerreiro.setEnabled(false);
            Jogador.setListaPersonagens(guerreiro);
        }
    }//GEN-LAST:event_btnGuerreiroActionPerformed

    private void btnLadraoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnLadraoActionPerformed
        if(ValidaQuantidadePersonagensVinculados()){
            btnLadrao.setEnabled(false);
            Jogador.setListaPersonagens(ladrao);
        }
    }//GEN-LAST:event_btnLadraoActionPerformed

    private void btnMagoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnMagoActionPerformed
        if(ValidaQuantidadePersonagensVinculados()){
            btnMago.setEnabled(false);
            Jogador.setListaPersonagens(mago);
        }
    }//GEN-LAST:event_btnMagoActionPerformed

    private void btnPaladinoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnPaladinoActionPerformed
        if(ValidaQuantidadePersonagensVinculados()){
            btnPaladino.setEnabled(false);
            Jogador.setListaPersonagens(paladino);
        }
    }//GEN-LAST:event_btnPaladinoActionPerformed

    private void btnAnimalActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAnimalActionPerformed
        if(ValidaQuantidadePersonagensVinculados()){
            btnAnimal.setEnabled(false);
            Jogador.setListaPersonagens(animal);
        }
    }//GEN-LAST:event_btnAnimalActionPerformed

    private void btnTrollActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnTrollActionPerformed
        if(ValidaQuantidadePersonagensVinculados()){
            btnTroll.setEnabled(false);
            Jogador.setListaPersonagens(troll);
        }
    }//GEN-LAST:event_btnTrollActionPerformed

    private void btnDragaoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDragaoActionPerformed
        if(ValidaQuantidadePersonagensVinculados()){
            btnDragao.setEnabled(false);
            Jogador.setListaPersonagens(dragao);
        }
    }//GEN-LAST:event_btnDragaoActionPerformed

    private void btnZumbiActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnZumbiActionPerformed
        if(ValidaQuantidadePersonagensVinculados()){
            btnZumbi.setEnabled(false);
            Jogador.setListaPersonagens(zumbi);
        }
    }//GEN-LAST:event_btnZumbiActionPerformed

    private void btnFinalizarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnFinalizarActionPerformed
        boolean achou = false;
        ArrayList<CLPartidaJogador> clj = (ArrayList<CLPartidaJogador>) Clpartida.getListaJogadores();
        for(CLJogador j : clj){
            if(j.getId() == Clpartida.getId())
                achou = true;
        }
        if(!achou){
            if(qtdPersonagens < 2){
            JOptionPane.showMessageDialog(null,"Gentileza escolher dois personagens!");
        }else{
            Clpartida.setListaJogadores(conversor.CLJogadorEmCLPartidaJogador(Jogador));
            
            
            JLista.setText("");
            for(CLJogador j : clj){
                String Comp = "A";
                ArrayList<CLPersonagem> clp = (ArrayList<CLPersonagem>) j.getListaPersonagens();
                for(CLPersonagem p : clp){
                    if(Comp.equals("A")){
                        Comp = p.getNome();
                    }else{
                        Comp = Comp+" - "+p.getNome();
                    }
                }
                JLista.append("Jogador: "+j.getNome()+" => ( "+Comp+" ) \n");
            }
            
            this.QtdJogadores.setText(Integer.toString((Integer.parseInt(this.QtdJogadores.getText())+1)));
            this.dispose();
        }
        }else{
            JOptionPane.showMessageDialog(null,"Jogador com o email informado já foi vinculado!");
            this.dispose();
        }
        
    }//GEN-LAST:event_btnFinalizarActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAnimal;
    private javax.swing.JButton btnDragao;
    private javax.swing.JButton btnFinalizar;
    private javax.swing.JButton btnGuerreiro;
    private javax.swing.JButton btnLadrao;
    private javax.swing.JButton btnMago;
    private javax.swing.JButton btnPaladino;
    private javax.swing.JButton btnTroll;
    private javax.swing.JButton btnZumbi;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JTextField txtEmail;
    private javax.swing.JTextField txtNome;
    // End of variables declaration//GEN-END:variables
}
