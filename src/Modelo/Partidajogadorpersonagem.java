package Modelo;
// Generated 06/11/2016 17:45:31 by Hibernate Tools 4.3.1



/**
 * Partidajogadorpersonagem generated by hbm2java
 */
public class Partidajogadorpersonagem  implements java.io.Serializable {


     private Integer id;
     private Partidajogador partidajogador;
     private Personagem personagem;

    public Partidajogadorpersonagem() {
    }

    public Partidajogadorpersonagem(Partidajogador partidajogador, Personagem personagem) {
       this.partidajogador = partidajogador;
       this.personagem = personagem;
    }
   
    public Integer getId() {
        return this.id;
    }
    
    public void setId(Integer id) {
        this.id = id;
    }
    public Partidajogador getPartidajogador() {
        return this.partidajogador;
    }
    
    public void setPartidajogador(Partidajogador partidajogador) {
        this.partidajogador = partidajogador;
    }
    public Personagem getPersonagem() {
        return this.personagem;
    }
    
    public void setPersonagem(Personagem personagem) {
        this.personagem = personagem;
    }




}


